#
# take raw files in extdata and make .rda files in data.  these become built-in datasets
#
library(poppr)

seed=1001 #randomly subsetting genotypes below, to replicate, use the same seed everytime
set.seed(seed)

snps = 1000

dips_psex <- genclone2genind(read.genalex("dips_psex.csv"))
save(file="../data/dips_psex.rda",dips_psex)

metaSNPpops = read.csv("meta-SNP.pops.csv")
save(file="../data/metaSNPpops.rda",metaSNPpops)

popmetaSSR = read.csv("popmeta.csv")
save(file="../data/popmetaSSR.rda",popmetaSSR)


####################################################
###### Here is the call logic used to make the
###### subset of the 60000 loci
####################################################

#read and save a version of the beagle file in R binary
SNPbeagle = read.table(gzfile("dips516ind.beagle.gz"),header=T,as.is=T)
save(file="../data/SNPbeagle.rda",SNPbeagle)

library(parallel)

callgeno = function(likes=c(.33,.33,.33),alleles=c(1,2),thresh=3)
{
  llikes = log(likes)
  m=which(llikes==min(llikes))[1]
  if (abs(2*(llikes[-m][1]-llikes[-m][2]))>=thresh)
  {
    b=which(llikes==max(llikes))
    paste0(paste0(rep(alleles[1],3-b)),paste0(rep(alleles[2],(b-1))),collapse='')
  } else NA
}

callSNP = function(r,alleles=c(1,2),thresh=3)
{
  n=length(r)/3
  d=cbind(
    starts = seq(1,by=3,length.out=n),
    stops = seq(3,by=3,length.out=n)
  )
  unlist(apply(d,1,function(x)
    {
    callgeno(likes=r[x[1]:x[2]],alleles=alleles,thresh=thresh)
  }))
}

numMiss = function(r)
{
n=length(r)/3
d=cbind(
  starts = seq(1,by=3,length.out=n),
  stops = seq(3,by=3,length.out=n)
)

sum(unlist(apply(d,1,function(x)
  {
    ifelse((max(r[x[1]:x[2]])<0.34),TRUE,FALSE)
  })))
}



nummiss <- unlist(mclapply(1:dim(SNPbeagle)[1],mc.cores=4,function(x)
{
  c(numMiss(SNPbeagle[x,-1:-3]))
}))

nmdf = data.frame(nummiss=nummiss,low=nummiss<10)
nmdf$chosen= FALSE
nmdf$chosen[sample(which(nmdf$low),snps*3,replace=F)]=TRUE

#lst <- mclapply(1:dim(SNPbeagle)[1],mc.cores=4,function(x)
lst <- mclapply(which(nmdf$chosen),mc.cores=4,function(x)
{
  c(SNPbeagle[x,1],callSNP(SNPbeagle[x,-1:-3],SNPbeagle[x,2:3]))
})

gc()

SNPcalls = do.call(rbind,lst)

rownames(SNPcalls) = SNPcalls[,1]
SNPcalls=SNPcalls[,-1]

SNPcalls2=gsub("0","A",SNPcalls)
SNPcalls2=gsub("1","T",SNPcalls2)
SNPcalls2=gsub("2","C",SNPcalls2)
SNPcalls2=gsub("3","G",SNPcalls2)
missamount = sort(rowSums(is.na(SNPcalls2)))[snps]
SNPcalls=SNPcalls2[rowSums(is.na(SNPcalls2))<=missamount,][1:snps,]


save(file="../data/SNPcalls.rda",SNPcalls)
gc()
